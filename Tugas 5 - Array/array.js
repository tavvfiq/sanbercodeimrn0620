//nomor 1
function range(startNum, finishNum) {
    var arr = new Array();
    if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--) {
            arr.push(i);
        }
    } else if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            arr.push(i);
        }
    } else {
        return -1;
    }
    return arr;
}
console.log(range(1,10));

//nomor 2
function rangeWithStep(startNum, finishNum, step) {
    var arr = new Array();
    if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i -= step) {
            arr.push(i);
        }
    } else if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i += step) {
            arr.push(i);
        }
    } else {
        return -1;
    }
    return arr;
}
console.log(rangeWithStep(1,10,2));

//nomor 3
function sum(startNum, finishNum, step) {
    var arr = new Array();
    if (step == null) {
        step = 1;
    }
    if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i -= step) {
            arr.push(i);
        }
    } else if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i += step) {
            arr.push(i);
        }
    } else {
        return -1;
    }
    return arr.reduce(function (a, b) {
        return a + b;
    });
}
console.log(sum(1,10));

//nomor 4
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(data) {
    for (var i = 0; i < data.length; i++) {
        console.log(`Nomor ID: ${data[i][0]}\nNama Lengkap: ${data[i][1]}\nTTL: ${data[i][2]} ${data[i][3]}\nHobi: ${data[i][4]}\n`);
    }
}

dataHandling(input);

//nomor 5
function balikKata(input) {
    var tmp = "";
    for (var i = 1; i <= input.length; i++) {
        tmp += input[input.length - i];
    }
    return tmp;
}

console.log(balikKata("Kasur Rusak"));

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];

function datahandling2(data) {
    data.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro");
    var ttl = data[3].split("/");
    var bulan = "";
    switch (ttl[1]) {
        case "01":
            bulan = "Januari";
            break;
        case "2":
            bulan = "Februari";
            break;
        case "03":
            bulan = "Maret";
            break;
        case "04":
            bulan = "April";
            break;
        case "05":
            bulan = "Mei";
            break;
        case "06":
            bulan = "Juni";
            break;
        case "07":
            bulan = "Juli";
            break;
        case "08":
            bulan = "Agustus";
            break;
        case "09":
            bulan = "September";
            break;
        case "10":
            bulan = "Oktober";
            break;
        case "11":
            bulan = "November";
            break;
        case "12":
            bulan = "Desember";
            break;
    }
    console.log(bulan);
    console.log(
        ttl.sort(function(a,b){
            return b-a;
    }));
    console.log(ttl.join("-"));
    console.log(data[1].slice(0,15));
}

datahandling2(input);