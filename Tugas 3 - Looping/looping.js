var i = 1;
while(i<=40){
    if(i<=20){
        if(i==1){
            console.log("INI LOOPING PERTAMA");
        }
        if(!(i%2)){
            console.log(`${i} - I love coding`);
        }
    } else {
        if(i==21){
            console.log("INI LOOPING KEDUA");
        }
        if(!((42-i)%2)){
            console.log(`${42-i} - I will become a mobile developer`);
        }
    }
    i++;
}
console.log("INI LOOPING KETIGA");
for(var i = 0;i<20;i++){
    if(i%2 && !(i%3)){
        console.log(`${i} - I Love Coding`);
    } else if(!(i%2)){
        console.log(`${i} - Berkualitas`);
    } else if(i%2){
        console.log(`${i} - Santai`);
    }
}
console.log("INI LOOPIN KEEMPAT");
for(var i=0;i<4;i++){
    var out = "";
    for(var j=0;j<8;j++){
        out += "#";
    }
    console.log(out);
}
console.log("INI LOOPIN KELIMA");
for(var i=1;i<=7;i++){
    var out="";
    for(var j=0;j<i;j++){
        out += "#";
    }
    console.log(out);
}
console.log("INI LOOPIN KEENAM");
for(var i=1;i<=8;i++){
    var out="";
    for(var j=1;j<=8;j++){
        if(!(i%2)){
            if(j%2){
                out+="#";
            } else {
                out+=" ";
            }
        } else {
            if(j%2){
                out+=" ";
            } else {
                out+="#";
            }
        }
    }
    console.log(out);
}