//nomor 1
function arrayToObject(arr) {
    var obj=[{}];
    for (var i = 0; i < arr.length; i++) {
        if (obj[i] == undefined) {
            obj[i] = {
                fname: "",
                lname: "",
                gender: "",
                age: 0
            };
        }
        obj[i]["fname"] = arr[i][0];
        obj[i]["lname"] = arr[i][1];
        obj[i]["gender"] = arr[i][2];
        if (arr[i][3] < 2020) {
            obj[i]["age"] = new Date().getFullYear() - arr[i][3];
        } else {
            obj[i]["age"] = "Invalid Birth Year";
        }
        console.log(`${i+1}. ${obj[i].fname} ${obj[i].lname}: `, obj[i]);
    }
}

// Driver Code
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

// nomor 2
function shoppingTime(memberId, money) {
    const obj = {
        memberId: "",
        money: 0,
        listPurchased: [],
        changeMoney: 0
    };
    if (memberId == null || memberId == "") {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money < 50000 || money == null) {
        return "Mohon maaf, uang tidak cukup";
    }
    obj["memberId"] = memberId;
    obj["money"] = money;
    while (money >= 50000) {
        if (money >= 1500000) {
            obj["listPurchased"].push("Sepatu brand Stacattu");
            money -= 1500000;
        } else if (money >= 500000) {
            obj["listPurchased"].push("Baju brand Zoro");
            money -= 500000;
        } else if (money >= 250000) {
            obj["listPurchased"].push("Baju brand H&N");
            money -= 250000;
        } else if (money >= 175000) {
            obj["listPurchased"].push("Sweater brand Uniklooh");
            money -= 175000;
        } else {
            obj["listPurchased"].push("Casing handphone");
            money -= 50000;
            break;
        }

    };
    obj["changeMoney"] = money;
    return obj;
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja

//nomor 3
function naikAngkot(arrPenumpang) {
    rute = ["A", "B", "C", "D", "E", "F"];
    if (arrPenumpang.length == 0) {
        return arrPenumpang;
    }
    const obj = [{}];
    for (var i = 0; i < arrPenumpang.length; i++) {
        if (obj[i] == undefined) {
            obj[i] = {
                penumpang: "",
                naikDari: "",
                tujuan: "",
                bayar: 0
            };
        }
        obj[i].penumpang = arrPenumpang[i][0];
        obj[i].naikDari = arrPenumpang[i][1];
        obj[i].tujuan = arrPenumpang[i][2];
        obj[i].bayar = rute.reduce(function (total, currVal, currIdx) {
            if (currVal == obj[i].naikDari) {
                return currIdx;
            } else if (currVal == obj[i].tujuan) {
                return (currIdx - total) * 2000;
            } else {
                return total+=0;
            }
        },0);
    }
    return obj;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]